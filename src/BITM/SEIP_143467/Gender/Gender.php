<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
//include("Database.php");
class Gender extends DB
{
    public $id = "";
    public $user_name = "";
    public $gender= "";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('gender',$data))
        {

            $this->gender=$data['gender'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->gender);


        $sql="insert into gender(user_name,gender)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [Gender: $this->gender ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }

}