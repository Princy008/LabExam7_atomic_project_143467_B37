<?php
namespace App\City;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
//include("Database.php");
class City extends DB
{
    public $id = "";
    public $user_name = "";
    public $city_name= "";

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    }
    public function  setData($data=NULL)
    {
        if (array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }
        if (array_key_exists('user_name',$data))
        {

            $this->user_name=$data['user_name'];
        }
        if (array_key_exists('city_name',$data))
        {

            $this->city_name=$data['city_name'];
        }
    }
    public function store()
    {
        $arrData=array($this->user_name,$this->city_name);


        $sql="insert into city(user_name,city_name)values (?,?)";
        echo $sql;

        $STH=$this->DBH->prepare($sql);
        $result= $STH->execute($arrData);

        if($result)
            Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [CityName: $this->city_name ] <br> Data Has Been Inserted Successfully!</h3></div>");
        else Message::message("<div id='msg'></div><h3 align='center'>[ UserName: $this->user_name ] , [CityName: $this->city_name ] <br> Data Has not  Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}